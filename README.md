# Sip
CLI for interacting with Gitea

[![Build Status](https://drone.gitea.com/api/badges/jolheiser/sip/status.svg)](https://drone.gitea.com/jolheiser/sip)

[Docs](docs.md)

### Features

Understands the concepts of an origin vs remote repository.  
By default, uses remotes `origin` and `upstream`.  
If no `upstream` repository is found, `upstream` becomes synonymous with `origin` for the sake of defaults.

* Configuration `sip config`
  * Change the default `origin` remote name `sip config origin`
  * Change the default `upstream` remote name `sip config upstrea`
* Tokens `sip tokens`
  * List current available access tokens
  * Generate a new token from CLI `sip token create <token>`
    * If `<token>` is present, add new access token, otherwise...
    * Authenticate with username/password to get a new token without leaving the terminal
  * Remove access tokens `sip tokens remove`
* Repository status `sip repo`
  * Get basic information about the `upstream` repository
* Issue search `sip issues`
  * Search issues based on keyword(s)
  * Create a new issue `sip issues create`
* Pull request search `sip pulls`
  * Search pull requests based on keyword(s)
  * Create a new pull request `sip pulls create`
  * Check pull request status (default based on current branch) `sip pulls status`
  * Checkout a pull request to test locally `sip pulls checkout`
* Release search `sip release`
  * Create a new release `sip release create`
  * Attach files to an existing release `sip release attach`
* Open item in a web browser
  * Repository `sip open` or `sip open owner/repo`
  * Issue or PR `sip open 1234` or `sip open owner/repo/1234`
  
### Search filters
Sip supports certain search filters for issues/PRs.  
Anything in the query that doesn't match one of the below filters will be sent as a keyword

* State `is:open is:closed is:merged` - only the last state in the query will be applied
* Author `author:jolheiser` - only the last author in the query will be applied
* Labels `label:bug label:feature` - all labels must apply to return results
* Milestone `mileston:v1.0.0` - only the last milestone in the query will be applied

e.g. `test is:open query author:jolheiser milestone:0.2.0` will search for issues/PRs with keywords `test query` that are `open`, authored by `jolheiser`, and in the `0.2.0` milestone.

## License

[MIT](LICENSE)
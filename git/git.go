package git

import (
	"os/exec"
	"strings"
)

// GetRepo returns a repositories parts
func GetRepo(remoteName string) ([]string, error) {
	cmd := exec.Command("git", "remote", "get-url", remoteName)
	out, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	remote := strings.TrimSpace(string(out))

	// SSH
	if strings.Contains(remote, "@") {
		remote = remote[strings.Index(remote, "@")+1:]
		parts := strings.Split(remote, ":")
		domain := "https://" + parts[0]
		ownerRepo := strings.Split(parts[1], "/")
		return []string{domain, ownerRepo[0], strings.TrimSuffix(ownerRepo[1], ".git")}, nil
	}

	// HTTP(S)
	parts := strings.Split(remote, "/")
	domain := parts[:len(parts)-2]
	ownerRepo := parts[len(parts)-2:]
	return []string{strings.Join(domain, "/"), ownerRepo[0], strings.TrimSuffix(ownerRepo[1], ".git")}, nil
}

// Branches returns current branch
func Branch() string {
	cmd := exec.Command("git", "branch", "--list", "--no-color")
	out, err := cmd.Output()
	if err != nil {
		return ""
	}

	for _, line := range strings.Split(string(out), "\n") {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "*") {
			return line[2:]
		}
	}

	return ""
}

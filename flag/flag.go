package flag

import (
	"fmt"
	"sync"

	"go.jolheiser.com/sip/config"
	"go.jolheiser.com/sip/git"

	"github.com/urfave/cli/v2"
)

type Remote struct {
	Name  string
	URL   string
	Owner string
	Repo  string
}

var (
	// Set via config
	Origin   Remote
	Upstream Remote

	// Set via flags
	URL   string
	Owner string
	Repo  string
	Token string

	// Set via Before
	OwnerRepoCtxSet bool

	Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "origin",
			Usage:       "The origin remote",
			Value:       config.Origin,
			Destination: &Origin.Name,
		},
		&cli.StringFlag{
			Name:        "upstream",
			Usage:       "The upstream remote",
			Value:       config.Upstream,
			Destination: &Upstream.Name,
		},
		&cli.StringFlag{
			Name:        "url",
			Aliases:     []string{"u"},
			Usage:       "The base URL to the Gitea instance",
			Value:       getUpstream().URL,
			Destination: &URL,
		},
		&cli.StringFlag{
			Name:        "owner",
			Aliases:     []string{"o"},
			Usage:       "The owner to target",
			Value:       getUpstream().Owner,
			Destination: &Owner,
		},
		&cli.StringFlag{
			Name:        "repo",
			Aliases:     []string{"r"},
			Usage:       "The repo to target",
			Value:       getUpstream().Repo,
			Destination: &Repo,
		},
		&cli.StringFlag{
			Name:        "token",
			Aliases:     []string{"t"},
			Usage:       "The access token to use (by name)",
			Destination: &Token,
		},
	}

	originOnce   sync.Once
	upstreamOnce sync.Once
)

func FullName() string {
	return fmt.Sprintf("%s/%s", Owner, Repo)
}

func FullURL() string {
	return fmt.Sprintf("%s/%s", URL, FullName())
}

func Before(ctx *cli.Context) error {
	OwnerRepoCtxSet = ctx.IsSet("owner") || ctx.IsSet("repo")
	return nil
}

func defRemote(remote, def string) string {
	if remote == "" {
		return def
	}
	return remote
}

func getUpstream() Remote {
	upstreamOnce.Do(func() {
		Upstream.URL, Upstream.Owner, Upstream.Repo = getOrigin().URL, getOrigin().Owner, getOrigin().Name
		upstream, err := git.GetRepo(defRemote(config.Upstream, "upstream"))
		if err == nil {
			Upstream.URL, Upstream.Owner, Upstream.Repo = upstream[0], upstream[1], upstream[2]
		}

	})
	return Upstream
}

func getOrigin() Remote {
	originOnce.Do(func() {
		Origin.URL, Origin.Owner, Origin.Name = "https://gitea.com", "jolheiser", "sip"
		origin, err := git.GetRepo(defRemote(config.Origin, "origin"))
		if err == nil {
			Origin.URL, Origin.Owner, Origin.Repo = origin[0], origin[1], origin[2]
		}
	})
	return Origin
}

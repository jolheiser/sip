package cmd

import (
	"errors"
	"fmt"

	"go.jolheiser.com/sip/config"
	"go.jolheiser.com/sip/flag"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

func NewApp(version string) *cli.App {
	app := cli.NewApp()
	app.Name = "Sip"
	app.Usage = "Command line tool to interact with Gitea"
	app.Version = version
	app.Commands = []*cli.Command{
		&Config,
		&Tokens,
		&Repo,
		&Issues,
		&Pulls,
		&Release,
		&Open,
	}
	app.Before = flag.Before
	app.Flags = flag.Flags
	app.EnableBashCompletion = true
	return app
}

func getToken(name string) string {
	for _, token := range config.Tokens {
		if name == token.Name {
			return token.Token
		}
	}
	return ""
}

func getClient(requireToken bool) (*gitea.Client, error) {
	if flag.Token != "" {
		return gitea.NewClient(flag.URL, gitea.SetToken(getToken(flag.Token)))
	}

	var token string
	if requireToken {
		if len(config.Tokens) == 0 {
			return nil, errors.New(color.Error.Wrap("No tokens found! Add one with #{sip token create}", color.New(color.FgMagenta)))
		}
		tokenMap := make(map[string]config.Token)
		opts := make([]string, len(config.Tokens))
		for idx, token := range config.Tokens {
			key := fmt.Sprintf("%s (%s)", token.Name, token.URL)
			tokenMap[key] = token
			opts[idx] = key
		}

		question := &survey.Select{Message: "This action requires an access token", Options: opts}
		var answer string

		if err := survey.AskOne(question, &answer); err != nil {
			return nil, err
		}

		token = tokenMap[answer].Token
	}

	return gitea.NewClient(flag.URL, gitea.SetToken(token))
}

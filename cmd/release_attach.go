package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"go.jolheiser.com/sip/flag"
	"go.jolheiser.com/sip/sdk"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

var ReleaseAttach = cli.Command{
	Name:   "attach",
	Usage:  "Attach files to a release",
	Action: doReleaseAttach,
	Subcommands: []*cli.Command{
		&ReleaseAttachRemove,
	},
}

func doReleaseAttach(_ *cli.Context) error {
	client, err := getClient(true)
	if err != nil {
		return err
	}

	releases, err := sdk.GetReleases(client, flag.Owner, flag.Repo, gitea.ListReleasesOptions{})
	if err != nil {
		return err
	}

	releaseNames := make([]string, len(releases))
	releaseMap := make(map[string]*gitea.Release)
	for idx, rel := range releases {
		releaseNames[idx] = rel.TagName
		releaseMap[rel.TagName] = rel
	}

	questions := []*survey.Question{
		{
			Name:     "release",
			Prompt:   &survey.Select{Message: "Release", Options: releaseNames},
			Validate: survey.Required,
		},
		{
			Name:   "attachments",
			Prompt: &survey.Multiline{Message: "Attachments", Help: "Enter file globs, each new line being a separate glob"},
		},
	}
	answers := struct {
		Release     string
		Attachments string
	}{}

	if err := survey.Ask(questions, &answers); err != nil {
		return err
	}

	release := releaseMap[answers.Release]
	files, err := fileGlobs(answers.Attachments)
	if err != nil {
		return err
	}
	if err := attachFiles(client, release.ID, files); err != nil {
		return err
	}

	info := color.Info
	cyan := color.New(color.FgCyan)
	fmt.Println(info.Format("Added"), cyan.Format(strconv.Itoa(len(files))), info.Format("attachments."))
	fmt.Println(cyan.Format(fmt.Sprintf("%s/releases/tag/%s", flag.FullURL(), release.TagName)))
	return nil
}

func fileGlobs(globList string) ([]string, error) {
	files := make([]string, 0)
	for _, glob := range strings.Split(globList, "\n") {
		glob = strings.TrimSpace(glob)
		matches, err := filepath.Glob(glob)
		if err != nil {
			return nil, err
		}
		files = append(files, matches...)
	}
	return files, nil
}

func attachFiles(client *gitea.Client, releaseID int64, files []string) error {
	beaver.Infof("Attachments:\n\t%s", strings.Join(files, "\n\t"))
	var confirm bool
	if err := survey.AskOne(&survey.Confirm{Message: "The above files will be attached, is the list correct?"}, &confirm); err != nil {
		return err
	}

	if confirm {
		for _, file := range files {
			fi, err := os.Open(file)
			if err != nil {
				return err
			}

			if _, _, err := client.CreateReleaseAttachment(flag.Owner, flag.Repo, releaseID, fi, fi.Name()); err != nil {
				return err
			}

			if err := fi.Close(); err != nil {
				return err
			}
		}
	}
	return nil
}

package cmd

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"

	"go.jolheiser.com/sip/config"
	"go.jolheiser.com/sip/flag"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/huandu/xstrings"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var PullsCheckout = cli.Command{
	Name:   "checkout",
	Usage:  "Checkout a pull request for testing",
	Action: doPullCheckout,
}

func doPullCheckout(ctx *cli.Context) error {
	client, err := getClient(true)
	if err != nil {
		return err
	}

	var issue *gitea.Issue
	questions := []*survey.Question{
		{
			Name:     "index",
			Prompt:   &survey.Input{Message: "Pull request number", Help: "Don't worry if you aren't sure! Just say -1 and we'll search for it inssipd!"},
			Validate: validatePRNum,
		},
	}
	prNum := struct {
		Index int64
	}{}
	if err := survey.Ask(questions, &prNum); err != nil {
		return err
	}

	if prNum.Index < 0 {
		var confirmed bool
		for !confirmed {
			iss, err := issuesSearch(ctx, true)
			if err != nil {
				return err
			}
			issue = iss
			confirmation := &survey.Confirm{Message: "Is this the pull request you want to checkout?"}
			if err := survey.AskOne(confirmation, &confirmed); err != nil {
				return err
			}
		}
	} else {
		iss, _, err := client.GetIssue(flag.Upstream.Owner, flag.Upstream.Repo, prNum.Index)
		if err != nil {
			return err
		}
		issue = iss
	}

	if issue == nil {
		return errors.New("no pull request selected")
	}

	branch := fmt.Sprintf("pr%d-%s", issue.Index, xstrings.ToKebabCase(issue.Title))
	cmd := exec.Command("git", "fetch", config.Upstream, fmt.Sprintf("pull/%d/head:%s", issue.Index, branch))
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		return err
	}

	beaver.Infof("Pull request successfully checked out. Switch to it using `git checkout %s`", branch)
	return nil
}

func validatePRNum(ans interface{}) error {
	if err := survey.Required(ans); err != nil {
		return err
	}
	if _, err := strconv.Atoi(ans.(string)); err != nil {
		return errors.New("pull request number must be an number")
	}
	return nil
}

package markdown

import (
	"strings"

	"github.com/charmbracelet/glamour"
	"github.com/kyokomi/emoji"
)

func normalizeNewlines(s string) string {
	s = strings.ReplaceAll(s, "\r\n", "\n")
	s = strings.ReplaceAll(s, "\r", "\n")
	return s
}

func Render(text string) (string, error) {
	return glamour.Render(emoji.Sprint(normalizeNewlines(text)), "dark")
}

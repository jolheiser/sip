package csv

import (
	"fmt"
	"sort"
	"strings"

	"code.gitea.io/sdk/gitea"
)

const TimeFormat = "Jan 2, 2006 at 03:04 PM"

func Issues(issues []*gitea.Issue) string {
	headers := "ID,Index,Author,Title,Body,Labels,Milestone,Assignee,State,Created,Updated,Closed\r\n"

	// Sort the issues coming in
	sort.Slice(issues, func(i, j int) bool {
		return issues[i].ID < issues[j].ID
	})

	rows := make([]string, 0)
	for _, issue := range issues {
		cols := make([]string, 12)
		cols[0] = fmt.Sprintf("%d", issue.ID)
		cols[1] = fmt.Sprintf("%d", issue.Index)
		if issue.Poster != nil {
			cols[2] = issue.Poster.UserName
		}
		cols[3] = issue.Title
		cols[4] = issue.Body
		labels := make([]string, 0)
		for _, lbl := range issue.Labels {
			labels = append(labels, lbl.Name)
		}
		cols[5] = strings.Join(labels, ",")
		if issue.Milestone != nil {
			cols[6] = issue.Milestone.Title
		}
		if issue.Assignee != nil {
			cols[7] = issue.Assignee.UserName
		}
		state := string(issue.State)
		if issue.PullRequest != nil && issue.PullRequest.HasMerged {
			state = "merged"
		}
		cols[8] = state
		cols[9] = issue.Created.Format(TimeFormat)
		cols[10] = issue.Updated.Format(TimeFormat)
		if issue.Closed != nil {
			cols[11] = issue.Closed.Format(TimeFormat)
		}
		for idx, col := range cols {
			cols[idx] = escape(col)
		}
		rows = append(rows, strings.Join(cols, ","))
	}
	return headers + strings.Join(rows, "\r\n")
}

func Releases(releases []*gitea.Release) string {
	headers := "ID,Tag Name,Target,Title,Body,Draft,Pre-Release,Created,Published,Publisher\r\n"

	// Sort the releases coming in
	sort.Slice(releases, func(i, j int) bool {
		return releases[i].ID < releases[j].ID
	})

	rows := make([]string, 0)
	for _, release := range releases {
		cols := make([]string, 10)
		cols[0] = fmt.Sprintf("%d", release.ID)
		cols[1] = release.TagName
		cols[2] = release.Target
		cols[3] = release.Title
		cols[4] = release.Note
		cols[5] = fmt.Sprintf("%t", release.IsDraft)
		cols[6] = fmt.Sprintf("%t", release.IsPrerelease)
		cols[7] = release.CreatedAt.Format(TimeFormat)
		cols[8] = release.PublishedAt.Format(TimeFormat)
		if release.Publisher != nil {
			cols[9] = release.Publisher.UserName
		}
		for idx, col := range cols {
			cols[idx] = escape(col)
		}
		rows = append(rows, strings.Join(cols, ","))
	}
	return headers + strings.Join(rows, "\r\n")
}

func escape(input string) string {
	return fmt.Sprintf(`"%s"`, strings.ReplaceAll(input, `"`, `""`))
}

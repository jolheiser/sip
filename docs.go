// +build docs

package main

import (
	"os"
	"strings"

	"go.jolheiser.com/sip/cmd"
)

func main() {
	app := cmd.NewApp("docs")

	fi, err := os.Create("docs.md")
	if err != nil {
		panic(err)
	}

	md, err := app.ToMarkdown()
	if err != nil {
		panic(err)
	}

	// Clean up the header
	md = md[strings.Index(md, "#"):]

	if _, err := fi.WriteString(md); err != nil {
		panic(err)
	}

	if err := fi.Close(); err != nil {
		panic(err)
	}
}

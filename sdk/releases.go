package sdk

import (
	"code.gitea.io/sdk/gitea"
)

// GetReleases returns all matching Releases from a Gitea instance
func GetReleases(client *gitea.Client, owner, repo string, opts gitea.ListReleasesOptions) ([]*gitea.Release, error) {
	releases := make([]*gitea.Release, 0)
	p := 1
	for {
		opts.Page = p
		list, _, err := client.ListReleases(owner, repo, opts)
		if err != nil {
			return releases, err
		}
		p++
		releases = append(releases, list...)

		if len(list) == 0 {
			break
		}
	}
	return releases, nil
}

// GetReleaseAttachments returns all attachments from a release
func GetReleaseAttachments(client *gitea.Client, owner, repo string, releaseID int64, opts gitea.ListReleaseAttachmentsOptions) ([]*gitea.Attachment, error) {
	attachments := make([]*gitea.Attachment, 0)
	p := 1
	for {
		opts.Page = p
		list, _, err := client.ListReleaseAttachments(owner, repo, releaseID, opts)
		if err != nil {
			return attachments, err
		}
		p++
		attachments = append(attachments, list...)

		// FIXME Seems paging doesn't want to work with release attachments
		if len(list) == 0 || len(list) < 50 {
			break
		}
	}
	return attachments, nil
}
